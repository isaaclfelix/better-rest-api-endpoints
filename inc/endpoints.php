<?php
define('BRAE_VERSION', 1);
define('BRAE_NAMESPACE', 'brae/v' . BRAE_VERSION);

// Endpoints
require('endpoints/posts.php');
require('endpoints/wp_query.php');

function brae_register_routes() {
  $posts_route = new BRAE_Posts_Route();
  $posts_route->register_routes();
}
add_action('rest_api_init', 'brae_register_routes');
?>
