<?php
class BRAE_WP_Query_Route extends WP_REST_Controller {
  // Register the routes for the objects of the controller
  public function register_routes() {
    $base = "/wp_query";
    // Default endpoint (brae/v1/wp_query)
    register_rest_route(BRAE_NAMESPACE, $base, array(
      array(
        'methods' =>  WP_REST_Server::READABLE,
        'callback' => array($this, 'get_items'),
        'permission_callback' => array($this, 'get_items_permissions_check'),
        'args' => array(
          // Author Parameters
          'author' => array(
            'default' => '',
            'validate_callback' => function($param, $request, $key) {
              return (is_int($param) || is_string($param));
            }
          ),
          'author_name' => array(
            'default' => '',
            'validate_callback' => function($param, $request, $key) {
              return is_string($request);
            }
          ),
          'author__in' => array(
            'default' => '',
            'validate_callback' => function($param, $request, $key) {
              return is_array($param);
            }
          ),
          'author__not_in' => array(
            'default' => '',
            'validate_callback' => function($param, $request, $key) {
              return is_array($param);
            }
          ),
          // Category Parameters
          // 'cat' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_int($param) || is_string($parm));
          //   }
          // ),
          // 'category_name' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'category__and' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'category__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'category__not_in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Tag Parameters
          // 'tag' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'tag_id' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'tag__and' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'tag__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'tag__not_in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'tag_slug__and' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'tag_slug__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Taxonomy Parameters
          // 'tax_query' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Search Parameter
          // 's' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // // Post & Page Parameters
          // 'p' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'name' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'title' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'page_id' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'pagename' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'post_parent' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'post_parent__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'post_parent__not_in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'post__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'post__not_in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // 'post_name__in' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Password Parameters
          // 'has_password' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // 'post_password' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // // Type Parameters
          // 'post_type' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_string($param) || is_array($param));
          //   }
          // ),
          // // Status Parameter
          // 'post_status' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_string($param) || is_array($param));
          //   }
          // ),
          // // Comment Parameters
          // 'comment_count' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_int($param) || is_array($param));
          //   }
          // ),
          // // Pagination Parameters
          // 'nopaging' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // 'posts_per_page' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'posts_per_archive_page' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'offset' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'paged' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'page' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'ignore_sticky_posts' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // // Order & Orderby Parameters
          // 'order' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_string($param) || is_array($param));
          //   }
          // ),
          // 'orderby' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return (is_string($param) || is_array($param));
          //   }
          // ),
          // // Date Parameters
          // 'year' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'monthnum' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'w' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'day' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'hour' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'minute' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'second' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'm' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_int($param);
          //   }
          // ),
          // 'date_query' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Custom Fields Parameters
          // 'meta_key' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'meta_value' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'meta_value_num' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_numeric($param);
          //   }
          // ),
          // 'meta_compare' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // 'meta_query' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_array($param);
          //   }
          // ),
          // // Permission Parameters
          // 'perm' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // // Mime Type Parameter
          // 'post_mime_type' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // ),
          // // Caching Parameters
          // 'cache_results' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // 'update_post_meta_cache' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // 'update_post_term_cache' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_bool($param);
          //   }
          // ),
          // // Return Fields Parameter
          // 'fields' => array(
          //   'default' => '',
          //   'validate_callback' => function($param, $request, $key) {
          //     return is_string($param);
          //   }
          // )
        )
      )
    ));
  }

  /**
   * Get a collection of items
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_Error|WP_REST_Response
   */
  public function get_items($request) {
    $params = $request->get_params();
    $args = array(
      // Author Parameters
      // 'author' => is_string($params['author']) ? implode(',', $params['author']) : $params['author'],
      // 'author_name' => $params['author_name'],
      // 'author__in' => $params['author__in'],
      // 'author__not_in' => $params['author__not_in'],
      // Category Parameters
      // 'cat' => $params['cat'],
      // 'category_name' => $params['category_name'],
      // 'category__and' => $params['category__and'],
      // 'category__in' => $params['category__in'],
      // 'category__not_in' => $params['category__not_in'],
      // Tag Parameters
      // 'tag' => $params['tag'],
      // 'tag_id' => $params['tag_id'],
      // 'tag__and' => $params['tag__and'],
      // 'tag__in' => $params['tag__in'],
      // 'tag__not_in' => $params['tag__not_in'],
      // 'tag_slug__and' => $params['tag_slug__and'],
      // 'tag_slug__in' => $params['tag_slug__in'],
      // 'tax_query' => $params['tax_query'],
      // Search Parameter
      // 's' => $params['s'],
      // Post & Page Parameters
      // 'p' => $params['p'],
      // 'name' => $params['name'],
      // 'title' => $params['title'],
      // 'page_id' => $params['page_id'],
      // 'pagename' => $params['pagename'],
      // 'post_parent' => $params['post_parent'],
      // 'post_parent__in' => $params['post_parent__in'],
      // 'post_parent__not_in' => $params['post_parent__not_in'],
      // 'post__in' => $params['post__in'],
      // 'post__not_in' => $params['post__not_in'],
      // 'post_name__in' => $params['post_name__in'],
      // Password Parameters
      // 'has_password' => $params['has_password'],
      // 'post_password' => $params['post_password'],
      // Type Parameters
      // 'post_type' => $params['post_type'],
      // Status Parmeters
      // 'post_status' => $params['post_status'],
      // Comment Parameters
      // 'comment_count' => $params['comment_count'],
      // Pagination Parameters
      // 'nopaging' => $params['nopaging'],
      // 'posts_per_page' => $params['posts_per_page'],
      // 'posts_per_archive_page' => $params['posts_per_archive_page'],
      // 'offset' => $params['offset'],
      // 'paged' => $params['paged'],
      // 'page' => $params['page'],
      // 'ignore_sticky_posts' => $params['ignore_sticky_posts'],
      // Order & Orderby Parameters
      // 'order' => $params['order'],
      // 'orderby' => $params['orderby'],
      // Date Parameters
      // 'year' => $params['year'],
      // 'monthnum' => $params['monthnum'],
      // 'w' => $params['w'],
      // 'day' => $params['day'],
      // 'hour' => $params['hour'],
      // 'minute' => $params['minute'],
      // 'second' => $params['second'],
      // 'm' => $params['m'],
      // 'date_query' => $params['date_query'],
      // Custom Fields Parameters
      // 'meta_key' => $params['meta_key'],
      // 'meta_value' => $params['meta_value'],
      // 'meta_value_num' => $params['meta_value_num'],
      // 'meta_compare' => $params['meta_compare'],
      // 'meta_query' => $params['meta_query'],
      // Permission Parameter
      // 'perm' => $params['perm'],
      // Mime Type Parameters
      // 'post_mime_type' => $params['post_mime_type'],
      // Caching Parameters
      // 'cache_results' => $params['cache_results'],
      // 'update_post_meta_cache' => $params['update_post_meta_cache'],
      // 'update_post_term_cache' => $params['update_post_term_cache'],
      // Return Fields Parameter
      // 'fields' => $params['fields']
    );
    $wp_query = new WP_Query($args);
    $items = $wp_query->get_posts();
    $data = array();
    foreach ($items as $item) {
      $itemdata = $this->prepare_item_for_response($item, $request);
      $data[] = $this->prepare_response_for_collection($itemdata);
    }
    return new WP_REST_Response($data, 200);
  }

  /**
   * Check if a given request has access to get items
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_Error|bool
   */
  public function get_items_permissions_check($request) {
    return true;
  }

  /**
   * Prepare the item for the REST response
   *
   * @param mixed $item WordPress representation of the item.
   * @param WP_REST_Request $request Request object.
   * @return mixed
   */
  public function prepare_item_for_response($item, $request) {
    return array(
      'ID' => $item->ID,
      'post_author' => $item->post_author,
      'post_name' => $item->post_name,
      'post_type' => $item->post_type,
      'post_title' => $item->post_title,
      'post_date' => $item->post_date,
      'post_content' => $item->post_content,
      'post_excerpt' => $item->post_excerpt,
      'post_status' => $item->post_status,
      'post_parent' => $item->post_parent
    );
  }
}
?>
