<?php
class BRAE_Posts_Route extends WP_REST_Controller {
  // Register the routes for the objects of the controller
  public function register_routes() {
    $base = "/posts";
    // Default endpoint (brae/v1/posts)
    register_rest_route(BRAE_NAMESPACE, $base, array(
      array(
        'methods' =>  WP_REST_Server::READABLE,
        'callback' => array($this, 'get_items'),
        'permission_callback' => array($this, 'get_items_permissions_check'),
        'args' => array(
          'posts_per_page' => array(
            'default' => 10,
            'validate_callback' => function($param, $request, $key) {
              return is_numeric($param);
            }
          ),
          'category' => array(
            'default' => 0,
            'validate_callback' => function($param, $request, $key) {
              return is_numeric($param) || is_array($param);
            }
          ),
          'include' => array(
            'default' => array(),
            'validate_callback' => function($param, $request, $key) {
              return is_array($param);
            }
          ),
          'exclude' => array(
            'default' => array(),
            'validate_callback' => function($param, $request, $key) {
              return is_array($param);
            }
          ),
          'supress_filters' => array(
            'default' => true,
            'validate_callback' => function($param, $request, $key) {
              return is_bool($param);
            }
          )
        )
      )
    ));

    // Single item endpoint (brae/v1/posts/(?P<id>[\d]+))
    register_rest_route(BRAE_NAMESPACE, $base . '/(?P<id>[\d]+)', array(
      array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => array($this, 'get_item'),
        'permission_callback' => array($this, 'get_item_permissions_check'),
        'args' => array(

        )
      )
    ));

    // Schema endpoint (brae/v1/posts/schema)
    // register_rest_route(BRAE_NAMESPACE, $base . '/schema', array(
    //   'methods' => WP_REST_Server::READABLE,
    //   'callback' => array($this, 'get_public_item_schema')
    // ));
  }

  /**
   * Get a collection of items
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_Error|WP_REST_Response
   */
  public function get_items($request) {
    $params = $request->get_params();
    $posts_per_page = $params['posts_per_page'];
    $category = is_array($params['category']) ? implode(',', $params['category']) : $params['category'];
    $include = $params['include'];
    $exclude = $params['exclude'];
    $supress_filters = $params['supress_filters'];
    $args = array(
      'posts_per_page' => $posts_per_page,
      'category' => $category,
      'post_type' => 'post',
      'post_status' => 'publish',
      'include' => $include,
      'exclude' => $exclude,
      'supress_filters' => $supress_filters
    );
    $items = get_posts($args);
    $data = array();
    foreach ($items as $item) {
      $itemdata = $this->prepare_item_for_response($item, $request);
      $data[] = $this->prepare_response_for_collection($itemdata);
    }
    return new WP_REST_Response($data, 200);
  }

  /**
   * Check if a given request has access to get items
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_Error|bool
   */
  public function get_items_permissions_check($request) {
    return true;
  }

  /**
   * Get one item from the collection
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_Error|WP_REST_Response
   */
  public function get_item($request) {
    // get parameters from request
    $params = $request->get_params();
    $item = get_post($params['id']);
    if ($item !== null) {
      $data = $this->prepare_item_for_response($item, $request);
      return new WP_REST_Response($data, 200);
    }
    else {
      return new WP_Error('post_not_existant', __('Post does not exist', 'better-rest-api-endpoints'));
    }
  }

  public function get_item_permissions_check($request) {
    return $this->get_items_permissions_check($request);
  }

  /**
   * Prepare the item for the REST response
   *
   * @param mixed $item WordPress representation of the item.
   * @param WP_REST_Request $request Request object.
   * @return mixed
   */
  public function prepare_item_for_response($item, $request) {
    return array(
      'ID' => $item->ID,
      'post_author' => $item->post_author,
      'post_name' => $item->post_name,
      'post_type' => $item->post_type,
      'post_title' => $item->post_title,
      'post_date' => $item->post_date,
      'post_content' => $item->post_content,
      'post_excerpt' => $item->post_excerpt,
      'post_status' => $item->post_status,
      'post_parent' => $item->post_parent
    );
  }
}
?>
