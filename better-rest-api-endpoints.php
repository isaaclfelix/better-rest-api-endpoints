<?php
/*
Plugin Name: Better Rest API endpoints
Plugin URI:
Description: A plugin to improve the default wordpress REST API
Version: 1.0
Author: Isaac L. Félix
Author URI: http://isaaclfelix.github.io
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: better-rest-api-endpoints
Domain Path:  /languages
*/

// Activation Hook
function brae_endpoints_activation() {

}
register_activation_hook(__FILE__, 'brae_endpoints_activation');

// Deactivation Hook
function brae_endpoints_deactivation() {

}
register_deactivation_hook(__FILE__, 'brae_endpoints_deactivation');

// Require endpoints
require('inc/endpoints.php');
?>
